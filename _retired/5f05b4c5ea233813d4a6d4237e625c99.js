/*---------------
 * jQuery Last.Fm Plugin by Engage Interactive
 * Examples and documentation at: http://labs.engageinteractive.co.uk/lastfm/
 * Copyright (c) 2009 Engage Interactive
 * Version: 1.0 (10-JUN-2009)
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 * Requires: jQuery v1.3 or later
---------------*/
!function(e){function t(e){return(e+"").replace(/\0/g,"0").replace(/\\([\\'"])/g,"$1")}e.fn.lastFM=function(a){var r={number:10,username:"willblackmore",apikey:"96e0589327a3f120074f74dbc8ec6443",artSize:"medium",noart:"images/noartwork.gif",onComplete:function(){}},i=e.extend({},r,a),n="http://ws.audioscrobbler.com/2.0/?method=user.getrecenttracks&user="+i.username+"&api_key="+i.apikey+"&limit="+i.number+"&format=json&callback=?",l=e(this),m=l.html();l.children(":first").remove(),"small"==i.artSize&&(imgSize=0),"medium"==i.artSize&&(imgSize=1),"large"==i.artSize&&(imgSize=2),this.each(function(){e.getJSON(n,function(a){e.each(a.recenttracks.track,function(e,a){art=""==a.image[1]["#text"]?i.noart:t(a.image[imgSize]["#text"]),url=t(a.url),song=a.name,artist=a.artist["#text"],album=a.album["#text"],l.append(m);var r=l.children(":eq("+e+")");r.find("[class=lfm_song]").append(song),r.find("[class=lfm_artist]").append(artist),r.find("[class=lfm_album]").append(album),r.find("[class=lfm_art]").append("<img src='"+art+"' alt='Artwork for "+album+"'/>"),r.find("a").attr("href",url).attr("title","Listen to "+song+" on Last.FM").attr("target","_blank"),e==i.number-1&&i.onComplete.call(this)})})})}}(jQuery);
;